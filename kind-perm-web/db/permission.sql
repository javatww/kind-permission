/*
SQLyog Ultimate v11.27 (32 bit)
MySQL - 5.7.11 : Database - kind_sample
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `sys_log` */

CREATE TABLE `sys_log` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `oper_code` varchar(64) NOT NULL COMMENT '操作代码',
  `type` varchar(32) DEFAULT NULL COMMENT '类型',
  `os` varchar(64) DEFAULT NULL COMMENT '系统信息',
  `browser` varchar(64) DEFAULT NULL COMMENT '浏览器信息',
  `ip_addr` varchar(32) DEFAULT NULL COMMENT 'ip地址',
  `mac` varchar(32) DEFAULT NULL COMMENT 'mac地址',
  `execute_time` int(10) DEFAULT NULL COMMENT '执行时间',
  `description` varchar(512) DEFAULT NULL COMMENT '描述',
  `request_param` varchar(512) DEFAULT NULL COMMENT '请求参数',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='用户日志';

/*Data for the table `sys_log` */

insert  into `sys_log`(`id`,`oper_code`,`type`,`os`,`browser`,`ip_addr`,`mac`,`execute_time`,`description`,`request_param`,`create_user`,`create_time`) values (1,'/wheat/order',NULL,'Windows','Chrome','127.0.0.1','B8-AE-ED-93-4C-74',134,NULL,'{}','admin','2016-07-25 14:32:26'),(2,'/system/role',NULL,'Windows','Chrome','127.0.0.1','B8-AE-ED-93-4C-74',273,NULL,'{}','admin','2016-07-25 14:32:34'),(3,'/wheat/order',NULL,'Windows','Chrome','127.0.0.1','B8-AE-ED-93-4C-74',136,NULL,'{}','admin','2016-07-25 15:22:14'),(4,'/wheat/order',NULL,'Windows','Chrome','127.0.0.1','B8-AE-ED-93-4C-74',83,NULL,'{}','admin','2016-07-25 15:22:38'),(5,'/wheat/order',NULL,'Windows','Chrome','127.0.0.1','B8-AE-ED-93-4C-74',148,NULL,'{}','admin','2016-07-25 15:38:05'),(6,'/wheat/order',NULL,'Windows','Chrome','127.0.0.1','B8-AE-ED-93-4C-74',2,NULL,'{}','admin','2016-07-25 15:42:09');

/*Table structure for table `sys_op_log` */

CREATE TABLE `sys_op_log` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sub_system` varchar(16) DEFAULT NULL COMMENT '子系统',
  `module` varchar(32) DEFAULT NULL COMMENT '模块',
  `operation` varchar(32) DEFAULT NULL COMMENT '操作名称',
  `actor` varchar(32) DEFAULT NULL COMMENT '创建者',
  `ip_addr` varchar(32) DEFAULT NULL COMMENT '操作IP',
  `content` varchar(512) DEFAULT NULL COMMENT '详细参数',
  `description` varchar(512) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_module` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

/*Data for the table `sys_op_log` */

insert  into `sys_op_log`(`id`,`sub_system`,`module`,`operation`,`actor`,`ip_addr`,`content`,`description`,`create_time`) values (1,'WHEAT','ORDER_PACKAGE','修改后台订单状态','admin','127.0.0.1','{\"dispStatus\":\"BACK\",\"orderId\":\"1000160517001655\",\"orderStatus\":\"NULLIFY\"}',NULL,'2016-07-27 10:55:51'),(2,'WHEAT','ORDER','修改OMS订单状态','admin','127.0.0.1','{\"orderId\":\"1000160517001655\",\"orderStatus\":\"NULLIFY\"}',NULL,'2016-07-27 10:55:51'),(3,'JOYSEED','ORDER_PACKAGE','修改后台订单状态','admin','127.0.0.1','{\"dispStatus\":\"BACK\",\"orderId\":\"1000160506001921\",\"orderStatus\":\"CANCELLED\"}',NULL,'2016-07-27 10:58:42'),(4,'JOYSEED','ORDER','修改OMS订单状态','admin','127.0.0.1','{\"orderId\":\"1000160506001921\",\"orderStatus\":\"CANCELLED\"}',NULL,'2016-07-27 10:58:42'),(39,'WHEAT','ORDER_PACKAGE','修改后台订单状态','admin','127.0.0.1','ORDER_PACKAGE [orderId=1000160517001396, orderStatus=PAID, dispStatus=INITIAL]','ORDER_PACKAGE [orderId=1000160517001396, orderStatus=PAID, dispStatus=DISTRIBUTED]','2016-07-29 15:21:05'),(40,'WHEAT','ORDER','修改OMS订单状态','admin','127.0.0.1','Order [orderId=1000160517001396, orderStatus=PAID]','Order [orderId=1000160517001396, orderStatus=COMPLETE]','2016-07-29 15:22:33'),(41,'JOYSEED','ORDER_PACKAGE','修改后台订单状态','admin','127.0.0.1','ORDER_PACKAGE [orderId=1000160510000600, orderStatus=RECEIVED, dispStatus=COMPLETE]','ORDER_PACKAGE [orderId=1000160510000600, orderStatus=PAID, dispStatus=INITIAL]','2016-07-29 15:23:36'),(42,'JOYSEED','ORDER','修改OMS订单状态','admin','127.0.0.1','Order [orderId=1000160510000600, orderStatus=RECEIVED]','Order [orderId=1000160510000600, orderStatus=]','2016-07-29 15:23:36'),(43,'JOYSEED','ORDER_PACKAGE','修改后台订单状态','admin','127.0.0.1','ORDER_PACKAGE [orderId=E07030601355907, orderStatus=RECEIVED, dispStatus=COMPLETE]','ORDER_PACKAGE [orderId=E07030601355907, orderStatus=PAID, dispStatus=INITIAL]','2016-07-29 15:24:33');

/*Table structure for table `sys_permission` */

CREATE TABLE `sys_permission` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` bigint(12) DEFAULT NULL COMMENT '父节点名称',
  `name` varchar(32) NOT NULL COMMENT '名称',
  `type` varchar(20) DEFAULT NULL COMMENT '类型:菜单or功能',
  `sort_no` int(8) DEFAULT NULL COMMENT '排序',
  `url` varchar(256) DEFAULT NULL COMMENT '菜单URL',
  `perm_code` varchar(50) DEFAULT NULL COMMENT '菜单编码',
  `icon` varchar(256) DEFAULT NULL COMMENT '图标',
  `status` varchar(8) NOT NULL DEFAULT '1' COMMENT '状态:1,有效 0,无效',
  `description` varchar(512) DEFAULT NULL COMMENT '描述',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人',
  `modify_user` varchar(32) DEFAULT NULL COMMENT '最后修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

/*Data for the table `sys_permission` */

insert  into `sys_permission`(`id`,`parent_id`,`name`,`type`,`sort_no`,`url`,`perm_code`,`icon`,`status`,`description`,`create_user`,`modify_user`,`modify_time`,`create_time`) values (1,NULL,'系统管理','F',10,'','','icon-standard-cog','1','',NULL,NULL,NULL,NULL),(2,1,'角色管理','F',0,'system/role','','icon-hamburg-my-account','1','',NULL,NULL,NULL,NULL),(3,1,'用户管理','F',0,'system/user','','icon-hamburg-user','1','',NULL,NULL,NULL,NULL),(4,2,'添加','O',NULL,'','sys:role:save','','1','角色添加',NULL,NULL,NULL,NULL),(5,2,'删除','O',NULL,'','sys:role:remove','','1','角色删除',NULL,NULL,NULL,NULL),(6,2,'修改','O',NULL,'','sys:role:change','','1','角色修改',NULL,NULL,NULL,NULL),(7,3,'添加','O',NULL,'','sys:user:save','','1','用户添加',NULL,NULL,NULL,NULL),(8,3,'删除','O',NULL,'','sys:user:remove','','1','用户删除',NULL,NULL,NULL,NULL),(12,1,'权限管理','F',5,'system/permission','','icon-hamburg-login','1','',NULL,NULL,NULL,NULL),(14,15,'数据源监控','F',6,'druid','','icon-hamburg-database','1','',NULL,NULL,NULL,NULL),(15,NULL,'系统监控','F',3,'','','icon-hamburg-graphic','1','',NULL,NULL,NULL,NULL),(16,3,'修改','O',NULL,'','sys:user:change','','1','用户修改',NULL,NULL,NULL,NULL),(25,12,'添加','O',NULL,'','sys:perm:save','','1','菜单添加',NULL,NULL,NULL,NULL),(26,12,'修改','O',NULL,'','sys:perm:change','','1','菜单修改',NULL,NULL,NULL,NULL),(27,12,'删除','O',NULL,'','sys:perm:remove','','1','菜单删除',NULL,NULL,NULL,NULL),(28,2,'查看','O',NULL,'','sys:role:view','','1','角色查看',NULL,NULL,NULL,NULL),(29,3,'查看','O',NULL,'','sys:user:view','','1','用户查看',NULL,NULL,NULL,NULL),(30,12,'查看','O',NULL,'','sys:perm:view','','1','权限查看',NULL,NULL,NULL,NULL),(33,3,'查看用户角色','O',NULL,'','sys:user:roleView','','1','查看用户角色',NULL,NULL,NULL,NULL),(34,2,'保存授权','O',NULL,'','sys:role:permChange','','1','保存修改的角色权限',NULL,NULL,'2016-06-22 16:39:02',NULL),(35,3,'修改用户角色','O',NULL,'','sys:user:roleChange','','1','修改用户拥有的角色',NULL,NULL,NULL,NULL),(36,2,'查看角色权限','O',NULL,'','sys:role:permView','','1','查看角色拥有的权限',NULL,NULL,NULL,NULL),(39,1,'菜单管理','F',4,'system/permission/menu','','icon-hamburg-old-versions','1','',NULL,NULL,NULL,NULL),(40,1,'字典管理','F',6,'system/dict',NULL,'icon-hamburg-address','0','数据字典管理',NULL,NULL,NULL,NULL),(45,39,'修改','O',NULL,'','sys:perm:change',NULL,'1','菜单管理',NULL,NULL,NULL,NULL),(58,39,'添加','O',NULL,'','sys:perm:save',NULL,'1','菜单管理',NULL,NULL,NULL,NULL),(59,39,'删除','O',NULL,'','sys:perm:remove',NULL,'1','菜单管理',NULL,NULL,NULL,NULL),(70,39,'查看','O',NULL,'','sys:perm:menu:view',NULL,'1','菜单管理',NULL,NULL,NULL,NULL),(71,NULL,'开发实例','F',2,'',NULL,'icon-hamburg-basket','1','商店',NULL,NULL,NULL,NULL),(72,71,'地址管理','F',NULL,'template/community',NULL,'icon-hamburg-product','1','商品管理',NULL,NULL,NULL,NULL),(74,1,'区域信息','F',7,'system/area',NULL,'icon-hamburg-world','0','管理行政区划',NULL,NULL,NULL,NULL),(80,2,'导出','O',NULL,'','sys:role:export',NULL,'1','角色导出数据',NULL,NULL,NULL,'2016-06-21 11:10:30'),(81,39,'导出','O',NULL,'','sys:perm:export',NULL,'1','菜单管理',NULL,NULL,NULL,'2016-06-21 11:26:56'),(83,82,'导入','O',NULL,'','joyseed:order:import',NULL,'1','',NULL,NULL,'2016-06-23 10:15:08','2016-06-22 16:53:20'),(84,77,'查看','O',NULL,'','wheat:order:view',NULL,'1','元麦配送员配送计划管理',NULL,NULL,'2016-06-23 10:13:46','2016-06-23 10:13:24'),(85,77,'打印','O',NULL,'','wheat:order:print',NULL,'1','打印',NULL,NULL,NULL,'2016-06-23 10:14:17'),(86,NULL,'工具管理','F',8,'',NULL,'icon-hamburg-config','1','工具管理',NULL,NULL,'2016-07-12 10:21:50','2016-07-12 09:59:00');

/*Table structure for table `sys_role` */

CREATE TABLE `sys_role` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(32) NOT NULL COMMENT '角色名称',
  `code` varchar(32) NOT NULL COMMENT '角色code',
  `description` varchar(512) DEFAULT NULL COMMENT '描述',
  `sort_no` int(8) DEFAULT NULL COMMENT '排序',
  `status` int(8) DEFAULT '1' COMMENT '状态',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人',
  `modify_user` varchar(32) DEFAULT NULL COMMENT '最后修改人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`name`,`code`,`description`,`sort_no`,`status`,`create_user`,`modify_user`,`create_time`,`modify_time`) values (1,'超级管理员','admin','超级管理员',0,1,'admin',NULL,'2016-06-15 15:16:00','2016-06-15 15:16:07'),(2,'临时用户','guest','测试',0,1,'admin',NULL,'2016-06-15 15:17:13','2016-06-15 15:17:17'),(4,'供应商','supper','测试供应商',1,1,'admin',NULL,NULL,NULL),(5,'摇滚甜心食品','sweet','摇滚甜心食品',NULL,1,NULL,NULL,NULL,NULL),(7,'元麦站长','wheat','元麦站长',NULL,1,NULL,NULL,NULL,NULL);

/*Table structure for table `sys_role_permission` */

CREATE TABLE `sys_role_permission` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` bigint(12) DEFAULT NULL COMMENT '角色id',
  `permission_id` bigint(12) DEFAULT NULL COMMENT '权限id',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人',
  `modify_user` varchar(32) DEFAULT NULL COMMENT '最后修改人',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_permission` */

insert  into `sys_role_permission`(`id`,`role_id`,`permission_id`,`create_user`,`modify_user`,`create_time`,`modify_time`) values (1,1,1,NULL,NULL,'2016-06-30 15:07:05',NULL),(2,1,2,NULL,NULL,'2016-06-30 15:07:05',NULL),(3,1,3,NULL,NULL,'2016-06-30 15:07:05',NULL),(4,1,4,NULL,NULL,'2016-06-30 15:07:05',NULL),(5,1,5,NULL,NULL,'2016-06-30 15:07:05',NULL),(6,1,6,NULL,NULL,'2016-06-30 15:07:05',NULL),(7,1,7,NULL,NULL,'2016-06-30 15:07:05',NULL),(8,1,8,NULL,NULL,'2016-06-30 15:07:05',NULL),(9,1,12,NULL,NULL,'2016-06-30 15:07:05',NULL),(10,1,14,NULL,NULL,'2016-06-30 15:07:05',NULL),(11,1,15,NULL,NULL,'2016-06-30 15:07:05',NULL),(12,1,16,NULL,NULL,'2016-06-30 15:07:05',NULL),(13,1,25,NULL,NULL,'2016-06-30 15:07:05',NULL),(14,1,26,NULL,NULL,'2016-06-30 15:07:05',NULL),(15,1,27,NULL,NULL,'2016-06-30 15:07:05',NULL),(16,1,28,NULL,NULL,'2016-06-30 15:07:05',NULL),(17,1,29,NULL,NULL,'2016-06-30 15:07:05',NULL),(18,1,30,NULL,NULL,'2016-06-30 15:07:05',NULL),(19,1,33,NULL,NULL,'2016-06-30 15:07:05',NULL),(20,1,34,NULL,NULL,'2016-06-30 15:07:05',NULL),(21,1,35,NULL,NULL,'2016-06-30 15:07:05',NULL),(22,1,36,NULL,NULL,'2016-06-30 15:07:05',NULL),(23,1,39,NULL,NULL,'2016-06-30 15:07:05',NULL),(24,1,45,NULL,NULL,'2016-06-30 15:07:05',NULL),(25,1,58,NULL,NULL,'2016-06-30 15:07:05',NULL),(26,1,59,NULL,NULL,'2016-06-30 15:07:05',NULL),(27,1,70,NULL,NULL,'2016-06-30 15:07:05',NULL),(28,1,71,NULL,NULL,'2016-06-30 15:07:05',NULL),(29,1,72,NULL,NULL,'2016-06-30 15:07:05',NULL),(30,1,75,NULL,NULL,'2016-06-30 15:07:05',NULL),(31,1,76,NULL,NULL,'2016-06-30 15:07:05',NULL),(32,1,77,NULL,NULL,'2016-06-30 15:07:05',NULL),(33,1,80,NULL,NULL,'2016-06-30 15:07:05',NULL),(34,1,81,NULL,NULL,'2016-06-30 15:07:05',NULL),(35,1,82,NULL,NULL,'2016-06-30 15:07:05',NULL),(36,1,83,NULL,NULL,'2016-06-30 15:07:05',NULL),(37,1,84,NULL,NULL,'2016-06-30 15:07:05',NULL),(38,1,85,NULL,NULL,'2016-06-30 15:07:05',NULL),(101,5,82,NULL,NULL,'2016-06-30 15:52:39',NULL),(102,5,83,NULL,NULL,'2016-06-30 15:52:39',NULL),(103,5,76,NULL,NULL,'2016-06-30 15:52:39',NULL),(104,7,77,NULL,NULL,'2016-06-30 15:53:47',NULL),(105,7,84,NULL,NULL,'2016-06-30 15:53:47',NULL),(106,7,85,NULL,NULL,'2016-06-30 15:53:47',NULL),(107,7,76,NULL,NULL,'2016-06-30 15:53:47',NULL),(108,2,2,NULL,NULL,'2016-07-01 14:36:41',NULL),(109,2,1,NULL,NULL,'2016-07-01 14:36:41',NULL),(110,2,4,NULL,NULL,'2016-07-01 14:36:41',NULL),(111,2,5,NULL,NULL,'2016-07-01 14:36:41',NULL),(112,2,6,NULL,NULL,'2016-07-01 14:36:41',NULL),(113,2,28,NULL,NULL,'2016-07-01 14:36:41',NULL),(114,2,34,NULL,NULL,'2016-07-01 14:36:41',NULL),(115,2,36,NULL,NULL,'2016-07-01 14:36:41',NULL),(116,2,80,NULL,NULL,'2016-07-01 14:36:41',NULL),(117,1,86,NULL,NULL,'2016-07-12 10:23:46',NULL),(118,1,87,NULL,NULL,'2016-07-26 16:21:12',NULL),(119,1,88,NULL,NULL,'2016-08-03 11:02:33',NULL),(120,7,88,NULL,NULL,'2016-08-03 11:02:47',NULL),(121,1,89,NULL,NULL,'2016-08-04 09:44:46',NULL);

/*Table structure for table `sys_user` */

CREATE TABLE `sys_user` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(32) NOT NULL COMMENT '用户名',
  `password` varchar(256) NOT NULL COMMENT '密码',
  `name` varchar(32) NOT NULL COMMENT '姓名',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `gender` int(8) DEFAULT NULL COMMENT '性别',
  `email` varchar(256) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机',
  `description` varchar(512) DEFAULT NULL COMMENT '描述',
  `status` int(8) DEFAULT NULL COMMENT '状态',
  `visit_count` int(8) DEFAULT '0' COMMENT '登录次数',
  `last_visit_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人',
  `modify_user` varchar(32) DEFAULT NULL COMMENT '最后修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`username`,`password`,`name`,`birthday`,`gender`,`email`,`mobile`,`description`,`status`,`visit_count`,`last_visit_time`,`create_user`,`modify_user`,`modify_time`,`create_time`) values (1,'admin','0192023a7bbd73250516f069df18b500','系统管理员','2016-06-12 00:00:00',1,'admin@m.com','13512345678','系统管理员',1,47,'2016-08-09 11:30:00',NULL,NULL,'2016-08-09 11:29:53','2016-06-12 16:39:03'),(3,'david','e10adc3949ba59abbe56e057f20f883e','魏先生','1991-06-05 00:00:00',0,'david@mcake.com','13761142302','测试数据',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'sweet','e10adc3949ba59abbe56e057f20f883e','甜心食品',NULL,1,'','','摇滚甜心食品',NULL,NULL,NULL,NULL,NULL,NULL,'2016-06-22 16:49:48'),(5,'wheat','e10adc3949ba59abbe56e057f20f883e','元麦站长',NULL,1,'','','元麦站长',NULL,NULL,NULL,NULL,NULL,NULL,'2016-06-29 18:14:48');

/*Table structure for table `sys_user_role` */

CREATE TABLE `sys_user_role` (
  `id` bigint(12) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(12) NOT NULL COMMENT '用户id',
  `role_id` bigint(12) NOT NULL COMMENT '角色id',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人',
  `modify_user` varchar(32) DEFAULT NULL COMMENT '最后修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `FK_USER_ROL_REFERENCE_ROLE` (`role_id`) USING BTREE,
  KEY `FK_USER_ROL_REFERENCE_USERS` (`user_id`) USING BTREE,
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`),
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`id`,`user_id`,`role_id`,`create_user`,`modify_user`,`modify_time`,`create_time`) values (2,4,5,'admin',NULL,NULL,'2016-06-22 20:24:58');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
